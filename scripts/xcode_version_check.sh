#!/bin/bash

# Extract Xcode version from the image name
xcode_version_from_image=$(echo "$CI_JOB_IMAGE" | sed -E 's/.*xcode-([0-9]+).*/\1/')     

# Get Xcode version using pkgutil
xcode_version_from_pkgutil_long=$(pkgutil --pkg-info=com.apple.pkg.CLTools_Executables | grep version)
xcode_version_from_pkgutil=$(echo "$xcode_version_from_pkgutil_long" | awk '{print $2}' | cut -c1-2)


echo "Xcode version from image: $xcode_version_from_image"
echo "Xcode version from pkgutil: $xcode_version_from_pkgutil_long"

# Compare the extracted Xcode versions
if [ "$xcode_version_from_image" = "$xcode_version_from_pkgutil" ]; then
    echo "Xcode version matches!"
else
    echo "Xcode version does not match."
    exit 1
fi